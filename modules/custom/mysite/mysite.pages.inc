<?php

/**
 * Page callbacks for the mysite module.
 */

/**
 * Page callback for artwork page.
 */
function mysite_artwork_page($section = NULL) {
  $output = '<ul class="artwork-options clear-block">';
  $output .= '<li>' . l(t('Digital Art'), 'artwork/digitalart', array('attributes' => array('class' => 'digitalart'))) . '</li>';
  $output .= '<li>' . l(t('Photography'), 'artwork/photography', array('attributes' => array('class' => 'photography'))) . '</li>';
  $output .= '<li>' . l(t('Fine Art'), 'artwork/fineart', array('attributes' => array('class' => 'fineart'))) . '</li>';
  $output .= '<li>' . l(t('Writing'), 'artwork/writing', array('attributes' => array('class' => 'writing'))) . '</li>';
  $output .= '</ul>';

  switch ($section) {
    case 'digitalart':
      drupal_set_title(t('Digital Art'));
      $output .= views_embed_view('images', 'default', mysite_get_latest_volume(), 'digitalart');
      break;
    case 'photography':
      drupal_set_title(t('Phtography'));
      $output .= views_embed_view('images', 'default', mysite_get_latest_volume(), 'photography');
      break;
    case 'fineart':
      drupal_set_title(t('Fine Art'));
      $output .= views_embed_view('images', 'default', mysite_get_latest_volume(), 'fineart');
      break;
    case 'writing':
      drupal_set_title(t('Writing'));
      $output .= views_embed_view('writing', 'default', mysite_get_latest_volume());
      break;
  }
  $output .= '<div class="archive-link">' . l(t('Archives'), 'archive') . '</div>';
  return $output;
}

/**
 * Page callback for contributors page.
 *
 * @todo:
 *   Code for getting category values (borrowed from content_allowed_values()
 *   is repetitive and sloppy.
 */
function mysite_contributors_page() {
  $output = '<div class="contributor-list">';

  $categories = array(
    'barrett' => t('Barrett Winners'),
    'featured' => t('Front Page Art'),
  );
  $settings = db_fetch_object(db_query("SELECT global_settings FROM {content_node_field} WHERE field_name = 'field_post_category'"));
  $global_settings = unserialize($settings->global_settings);
  $list = explode("\n", $global_settings['allowed_values']);
  foreach ($list as $opt) {
    // Sanitize the user input with a permissive filter.
    $opt = content_filter_xss($opt);
    if (strpos($opt, '|') !== FALSE) {
      list($key, $value) = explode('|', $opt);
      $categories[$key] = (isset($value) && $value !== '') ? $value : $key;
    }
  }
  $settings = db_fetch_object(db_query("SELECT global_settings FROM {content_node_field} WHERE field_name = 'field_artwork_category'"));
  $global_settings = unserialize($settings->global_settings);
  $list = explode("\n", $global_settings['allowed_values']);
  foreach ($list as $opt) {
    // Sanitize the user input with a permissive filter.
    $opt = content_filter_xss($opt);
    if (strpos($opt, '|') !== FALSE) {
      list($key, $value) = explode('|', $opt);
      $categories[$key] = (isset($value) && $value !== '') ? $value : $key;
    }
  }
//  $output .= '<pre>' . htmlspecialchars(print_r($categories, 1)) . '</pre>';

  $result = db_query("SELECT node.nid AS nid,
    node_data_field_artwork_category.field_artwork_category_value AS artwork_category_value,
    node.type AS node_type,
    node.vid AS node_vid,
    node_data_field_post_category.field_post_category_value AS post_category_value,
    node_data_field_author.field_author_value AS author_value
    FROM node node
    LEFT JOIN content_field_author node_data_field_author ON node.vid = node_data_field_author.vid
    LEFT JOIN content_type_artwork node_data_field_artwork_category ON node.vid = node_data_field_artwork_category.vid
    LEFT JOIN content_type_post node_data_field_post_category ON node.vid = node_data_field_post_category.vid
    WHERE (node_data_field_author.field_author_value IS NOT NULL) AND (node.status <> 0)");
  $rows = array();
  while ($row = db_fetch_object($result)) {
//    $output .= sprintf(
//      '<p>nid=%s type="%s" author="%s" artwork_category="%s" post_category="%s"</p>',
//      $row->nid, $row->node_type, $row->author_value, $row->artwork_category_value, $row->post_category_value
//    );
    $category = NULL;
    switch ($row->node_type) {
      case 'artwork':
        $category = $row->artwork_category_value;
        $weight = 2;
        break;
      case 'post':
        $category = $row->post_category_value;
        $weight = 0;
        break;
      case 'barrett_winners':
        $category = 'barrett';
        $weight = 1;
        break;
      case 'rotator':
        $category = 'featured';
        $weight = 3;
        break;
    }
    if ($category) {
      $rows[$weight . $category . $row->author_value] = $category . ':'. $row->author_value;
    }
  }
  ksort($rows);
//  $output .= '<pre>' . htmlspecialchars(print_r($rows, 1)) . '</pre>';
  $last = NULL;
  foreach($rows as $row) {
    list($category, $value) = explode (':', $row);
    if ($last !== $category) {
      $output .= '<h3 class="title">' . $categories[$category] . '</h3>';
      $last = $category;
    }
    $output .= '<div class="row">' . $value . '</div>';
  }
  $output .= '</div>';
  return $output;
}
